﻿using System;
using System.Linq;
using System.Collections.Generic;
using StatService.StochasticDifferentialEquation;
using StatService.SDESolver;
using StatService.SDESolver.Algorithm;
using StatService.StochasticProcess;
using StatService.SDESolverBuilder;


namespace StatService.SDESolver
{
    public static class SDESolverFactory
    {
        public static ISDESolver GetSDESolver(
            SDE sde, 
            SDESolverCondition cdn, 
            Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker)
        {
            if(cdn.ESolver == ESDESolver.EularMaruyama)
            {
                return GetEularMaruyamaSolver(sde, cdn, timeGridMaker);
            }
            if(cdn.ESolver == ESDESolver.Milstein)
            {
                return GetMilsteinSolver(sde, cdn, timeGridMaker);
            }

            throw new NotImplementedException();
        }

        private static ISDESolver GetMilsteinSolver(SDE sde, SDESolverCondition cdn, Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker)
        {
            var emBuilder = new MilsteinBuilder();
            emBuilder.AddSDE(sde);
            emBuilder.AddSDSolverCondition(cdn);
            emBuilder.AddTimeGridMaker(timeGridMaker);
            return emBuilder.BuildSolver();
        }

        private static ISDESolver GetEularMaruyamaSolver(
            SDE sde,
            SDESolverCondition cdn,
            Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker)
        {
            var emBuilder = new EularMaruyamaBuilder();
            emBuilder.AddSDE(sde);
            emBuilder.AddSDSolverCondition(cdn);
            emBuilder.AddTimeGridMaker(timeGridMaker);
            return emBuilder.BuildSolver();
        }
    }
}
