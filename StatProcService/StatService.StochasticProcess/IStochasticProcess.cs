﻿using System.Collections.Generic;
using System.Linq;

namespace StatService.StochasticProcess
{
    public interface IStochasticProcess
    {
        EStochasticProcess Type { get; }
        Parameter Parameter { get; }
    }
}
