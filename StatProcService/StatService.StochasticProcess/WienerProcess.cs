﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StatService.Utils;
using MathNet.Numerics.LinearAlgebra;
using Accord.Statistics.Distributions.Univariate;
using Accord.Statistics.Distributions.Multivariate;

namespace StatService.StochasticProcess
{
    /// <summary>
    /// Non-Correlated Wiener Process
    /// </summary>
    public class WienerProcess : IStochasticProcess
    {
        public WienerProcess(Parameter param, int dim = 1)
        {
            Parameter = param;
            Dim = dim;
            Type = EStochasticProcess.WienerProcess;
        }

        public Parameter Parameter { get; }

        public int Dim { get; }

        public EStochasticProcess Type { get; }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(WienerProcess))
            {
                return Parameter.Equals(((WienerProcess)obj).Parameter);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return Parameter.GetHashCode();
        }

        public override string ToString()
        {
            return "WienerProcess:" + Parameter.ToString();
        }

        public static MultivariateNormalDistribution operator -(WienerProcess wt, WienerProcess ws)
        {
            var dt = wt.Parameter.Time - ws.Parameter.Time;
            var means = Enumerable.Range(0, wt.Dim).Select(d => 0.0);

            var identity = (Matrix<double>.Build.DenseIdentity(wt.Dim) * dt).ToRowArrays();
            var dist = new MultivariateNormalDistribution(means.ToArray(), identity);

            return dist;
        }
    }
}
