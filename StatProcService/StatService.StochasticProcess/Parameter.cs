﻿using System;

namespace StatService.StochasticProcess
{
    public class Parameter
    {
        public Parameter(double time) => this.Time = time;

        public double Time { get; }

        public bool Equals(Parameter parameter)
        {
            if (Math.Abs(Time - parameter.Time) < 1.0e-10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Parameter))
            {
                return false;
            }
            else
            {
                return Equals((Parameter)obj);
            }
        }

        public override int GetHashCode()
        {
            return Time.GetHashCode();
        }

        public override string ToString()
        {
            return string.Join(", ", Time);
        }

        public static Parameter operator +(Parameter p1, Parameter p2)
        {
            return new Parameter(p1.Time + p2.Time);
        }
        public static Parameter operator -(Parameter p1, Parameter p2)
        {
            return new Parameter(p1.Time - p2.Time);
        }

        public static Parameter operator *(Parameter p1, double d)
        {
            return new Parameter(p1.Time * d);
        }
        public static Parameter operator /(Parameter p1, double d)
        {
            return new Parameter(p1.Time / d);
        }

        public static Parameter operator *(double d, Parameter p1)
        {
            return new Parameter(p1.Time * d);
        }




    }
}
