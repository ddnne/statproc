﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StatService.SDESolver;
using StatService.SDESolver.Algorithm;
using StatService.StochasticProcess;
using StatService.StochasticDifferentialEquation;
using StatService.Utils;


namespace StatService.Tests.TestSDESolverFactory
{
    [TestClass]
    public class TestSDESolverFactory
    {
        private IEnumerable<Parameter> TestTimeGridMaker(Parameter start, Parameter end)
        {
            return new List<Parameter> { start, end };
        }

        private Func<IEnumerable<Complex>, double, Complex>[] TestArrayFuncOfDtTerm()
        {
            return new Func<IEnumerable<Complex>, double, Complex>[]
            {
                delegate (IEnumerable<Complex> xs, double t){return new Complex(1, 1); }
            };
        }

        private FunctionVector<Input> TestVectorFuncOfDtTerm()
        {
            var tmp = new Func<IEnumerable<Complex>, double, Complex>[]
            {
                delegate (IEnumerable<Complex> xs, double t){return new Complex(1, 1); }
            };
            return new FunctionVector<Input>(UtilFunctions.ConvertArgToInput(tmp));
        }

        private Func<IEnumerable<Complex>, double, Complex>[][] Test2dArrayFuncOfRandomTerm()
        {
            return new Func<IEnumerable<Complex>, double, Complex>[1][]
            {
                new Func<IEnumerable<Complex>, double, Complex>[]
                {
                    delegate (IEnumerable<Complex> xs, double t)
                    {
                        return new Complex(1, 1);
                    }
                }
            };
        }

        private FunctionMatrix<Input> TestMatrixFuncOfRandomTerm()
        {
            var tmp = new Func<IEnumerable<Complex>, double, Complex>[1][]
            {
                new Func<IEnumerable<Complex>, double, Complex>[]
                {
                    delegate (IEnumerable<Complex> xs, double t)
                    {
                        return new Complex(1, 1);
                    }
                }
            };
            return new FunctionMatrix<Input>(UtilFunctions.ConvertArgToInput(tmp));
        }

        [TestMethod]
        public void TestGetEularMaruyamaSolverByIEnumerableFunctions()
        {
            Parameter start = new Parameter(0);
            Parameter end = new Parameter(1);
            var cdn = new SDESolverCondition(ESDESolver.EularMaruyama, end);
            var initial = new Complex[] { new Complex(1, 1) };
            var f = TestArrayFuncOfDtTerm();
            var l = Test2dArrayFuncOfRandomTerm();
            var sde = new SDE(start, initial, f, l);

            var solver = SDESolverFactory.GetSDESolver(sde, cdn, TestTimeGridMaker);
            Assert.IsTrue(solver is EularMaruyamaSolver);
        }

        [TestMethod]
        public void TestGetEularMaruyamaSolverByVectorMatrixFunctions()
        {
            Parameter start = new Parameter(0);
            Parameter end = new Parameter(1);
            var cdn = new SDESolverCondition(ESDESolver.EularMaruyama, end);
            var initial = MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfEnumerable(new Complex[] { new Complex(1, 1) });
            var f = TestVectorFuncOfDtTerm();
            var l = TestMatrixFuncOfRandomTerm();
            var sde = new SDE(start, initial, f, l);

            var solver = SDESolverFactory.GetSDESolver(sde, cdn, TestTimeGridMaker);
            Assert.IsTrue(solver is EularMaruyamaSolver);
        }
    }
}
