﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using StatService.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StatService.Tests.TestUtils
{
    [TestClass]
    public class TestFuntionMatrix
    {
        [TestMethod]
        public void TestFunctionConstructor()
        {
            var func = new List<List<Func<double, Complex>>> { new List<Func<double, Complex>>
            {
                delegate (double t) { return new Complex(t, t * t); } }
            };
            var fv = new FunctionMatrix<double>(func);

            double time = 1.5;
            var act = fv.GetValues(time)[0, 0];
            Assert.AreEqual(time, act.Real, delta: 1.0e-10);
            Assert.AreEqual(time * time, act.Imaginary, delta: 1.0e-10);
        }

        [TestMethod]
        public void TestConstantConstructor()
        {
            var func = new List<List<Complex>> { new List<Complex>
            {
                new Complex(1, 2)
            } };

            var t = 1.0e+99;

            var fv = new FunctionMatrix<double>(func);
            var act = fv.GetValues(t)[0, 0];
            Assert.AreEqual(1, act.Real, delta: 1.0e-10);
            Assert.AreEqual(2, act.Imaginary, delta: 1.0e-10);

            fv = new FunctionMatrix<double>(func.Select(v => v.ToArray()));
            Assert.AreEqual(1, act.Real, delta: 1.0e-10);
            Assert.AreEqual(2, act.Imaginary, delta: 1.0e-10);

            fv = new FunctionMatrix<double>(func.Select(v => v.ToArray()).ToArray());
            Assert.AreEqual(1, act.Real, delta: 1.0e-10);
            Assert.AreEqual(2, act.Imaginary, delta: 1.0e-10);
        }

        [TestMethod]
        public void TestRealImaginaryValues()
        {
            var func = new List<List<Func<double, Complex>>> { new List<Func<double, Complex>>
            {
                delegate (double t) { return new Complex(t, t * t); } }
            };
            var fv = new FunctionMatrix<double>(func);

            double time = 1.5;
            var act = new Complex(fv.GetRealValues(time)[0, 0], fv.GetImaginaryValues(time)[0, 0]);
            Assert.AreEqual(time, act.Real, delta: 1.0e-10);
            Assert.AreEqual(time * time, act.Imaginary, delta: 1.0e-10);
        }

        [TestMethod]
        public void TestAddOperator()
        {
            var f1 = new List<List<Func<double, Complex>>>
            {
                new List<Func<double, Complex>>
                {
                    delegate (double t) { return new Complex(Math.Sin(t), Math.Cos(t)); }
                }
            };
            var f2 = new List<List<Func<double, Complex>>> {
                new List<Func<double, Complex>>
                {
                    delegate (double t) { return new Complex(Math.Log(t), Math.Exp(t)); }
                }
            };

            var fv1 = new FunctionMatrix<double>(f1);
            var fv2 = new FunctionMatrix<double>(f2);

            var fv = fv1 + fv2;

            double time = 1.5;
            double expRe = Math.Sin(time) + Math.Log(time);
            double expIm = Math.Cos(time) + Math.Exp(time);

            var value = fv.GetValues(time)[0, 0];
            Assert.AreEqual(expRe, value.Real, delta: 1.0e-10);
            Assert.AreEqual(expIm, value.Imaginary, delta: 1.0e-10);
        }

        [TestMethod]
        public void TestSubOperator()
        {
            var f1 = new List<List<Func<double, Complex>>>
            {
                new List<Func<double, Complex>>
                {
                    delegate (double t) { return new Complex(Math.Sin(t), Math.Cos(t)); }
                }
            };
            var f2 = new List<List<Func<double, Complex>>> {
                new List<Func<double, Complex>>
                {
                    delegate (double t) { return new Complex(Math.Log(t), Math.Exp(t)); }
                }
            };

            var fv1 = new FunctionMatrix<double>(f1);
            var fv2 = new FunctionMatrix<double>(f2);

            var fv = fv1 - fv2;

            double time = 1.5;
            double expRe = Math.Sin(time) - Math.Log(time);
            double expIm = Math.Cos(time) - Math.Exp(time);

            var value = fv.GetValues(time)[0, 0];
            Assert.AreEqual(expRe, value.Real, delta: 1.0e-10);
            Assert.AreEqual(expIm, value.Imaginary, delta: 1.0e-10);
        }

        [TestMethod]
        public void TestMulOperator()
        {
            var f1 = new List<List<Func<double, Complex>>>
            {
                new List<Func<double, Complex>>
                {
                    delegate (double t) { return new Complex(Math.Sin(t), Math.Cos(t)); }
                }
            };
            var f2 = new List<List<Func<double, Complex>>> {
                new List<Func<double, Complex>>
                {
                    delegate (double t) { return new Complex(Math.Log(t), Math.Exp(t)); }
                }
            };

            var fv1 = new FunctionMatrix<double>(f1);
            var fv2 = new FunctionMatrix<double>(f2);

            var f = fv1 * fv2;

            double time = 1.5;
            double expRe = Math.Sin(time) * Math.Log(time) - Math.Cos(time) * Math.Exp(time);
            double expIm = Math.Cos(time) * Math.Log(time) + Math.Sin(time) * Math.Exp(time);

            var value = f.GetValues(time)[0, 0];
            Assert.AreEqual(expRe, value.Real, delta: 1.0e-10);
            Assert.AreEqual(expIm, value.Imaginary, delta: 1.0e-10);
        }
    }
}
