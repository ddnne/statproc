using Microsoft.VisualStudio.TestTools.UnitTesting;
using StatService.StochasticProcess;
using System.Collections.Generic;
using System.Linq;

namespace StatService.Tests.TestStochasticProcess
{
    [TestClass]
    public class TestParameter
    {
        [TestMethod]
        public void TestEquals()
        {
            var ps1 = new List<double> { 0, 1, 2 };
            var same_cases = new List<double> { 0, 1, 2 + 1.0e-11 };

            var cases = ps1.Zip(same_cases, (p1, p2) => new { p1, p2 });

            foreach (var testcase in cases)
            {
                var param1 = new Parameter(testcase.p1);

                var same_param = new Parameter(testcase.p2);

                Assert.IsTrue(param1.Equals(same_param));
            }
        }

        public void CompareNotParameter()
        {
            var p1 = new List<double> { 0, 1 };

            foreach (var p in p1)
            {
                var param1 = new Parameter(p);

                Assert.IsTrue(!param1.Equals("a"));

            }
        }

        [TestMethod]
        public void DifferentValue()
        {
            var p1 = 0;
            var param1 = new Parameter(p1);

            var duff_value = 1;
            var diff_value_param = new Parameter(duff_value);

            Assert.IsFalse(param1.Equals(diff_value_param));
        }
    }
}