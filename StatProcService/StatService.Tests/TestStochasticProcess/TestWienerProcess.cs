﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StatService.StochasticProcess;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Numerics;

namespace StatService.Tests.TestStochasticProcess
{
    [TestClass]
    public class TestWienerProcess
    {
        [TestMethod]
        public void TestEquals()
        {
            var wt = new WienerProcess(new Parameter(0));
            var ws = new WienerProcess(new Parameter(0));

            Assert.IsTrue(wt.Equals(ws));
            Assert.IsFalse(wt.Equals("aaa"));
        }
    }

    [TestClass]
    public class TestOpertor
    {
        [TestMethod]
        public void TestMinusOperator()
        {
            var wt = new WienerProcess(new Parameter(1));
            var w0 = new WienerProcess(new Parameter(0));

            var dist = (wt - w0);

            Assert.AreEqual(0, dist.Mean[0]);
            Assert.AreEqual(1.0, dist.Covariance[0, 0], delta: 1.0e-14);
        }

        [ExpectedException(typeof(Accord.NonPositiveDefiniteMatrixException))]
        [TestMethod]
        public void MinusTimeDiff()
        {
            var wt = new WienerProcess(new Parameter(1));
            var w0 = new WienerProcess(new Parameter(0));

            var dist = (w0 - wt);
        }
    }
}