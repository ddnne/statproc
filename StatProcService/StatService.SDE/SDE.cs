﻿using System;
using System.Collections.Generic;
using System.Linq;
using StatService.StochasticProcess;
using System.Numerics;
using MathNet.Numerics.LinearAlgebra;
using StatService.Utils;

namespace StatService.StochasticDifferentialEquation
{
    /// <summary>
    /// SDE: dx_i_t = F_i(x_t, t)dt + L_ij(x_t, t)dW_j_t
    /// x_i_t         : i'th of vector x_t
    /// S_i_t         : i'th of Wiener Process W_t
    /// L_ij(x_t, t)  : ij'th of the coeff matrix of W_t
    /// Q_ij_t        : correlation matrix of S_t (identity if q = null)
    /// 
    /// consider non-correlated W_t only
    /// </summary>
    public class SDE
    {
        public SDE(
            Parameter start,
            Complex[] initial,
            Func<IEnumerable<Complex>, double, Complex>[] f,
            Func<IEnumerable<Complex>, double, Complex>[][] l
            )
        {
            Start = start;
            InitialValues = MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfEnumerable(initial);
            F = new FunctionVector<Input>(UtilFunctions.ConvertArgToInput(f));
            L = new FunctionMatrix<Input>(UtilFunctions.ConvertArgToInput(l));
        }

        public SDE(
            Parameter start,
            MathNet.Numerics.LinearAlgebra.Vector<Complex> initial,
            FunctionVector<Input> f,
            FunctionMatrix<Input> l
            )
        {
            Start = start;
            InitialValues = initial;
            F = f;
            L = l;
        }

        public SDE(
            Parameter start,
            Complex[] initial,
            FunctionVector<Input> f,
            FunctionMatrix<Input> l
            )
        {
            Start = start;
            InitialValues = MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfEnumerable(initial);
            F = f;
            L = l;
        }

        public Parameter Start { get; }

        public MathNet.Numerics.LinearAlgebra.Vector<Complex> InitialValues { get; }

        public FunctionVector<Input> F { get; }

        public FunctionMatrix<Input> L { get; }
    }
}
