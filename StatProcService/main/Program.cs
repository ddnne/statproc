﻿using System;
using System.Numerics;
using System.Linq;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;
using StatService.StochasticDifferentialEquation;
using StatService.StochasticProcess;
using StatService.SDESolver;
using StatService.Utils;


namespace main
{
    internal class ResultSaver
    {
        public ResultSaver(IEnumerable<IEnumerable<MathNet.Numerics.LinearAlgebra.Vector<Complex>>> results, IEnumerable<Parameter> timeGrid)
        {
            Results = results.ToList();
            TimeGrid = timeGrid;
        }

        public List<IEnumerable<MathNet.Numerics.LinearAlgebra.Vector<Complex>>> Results { get; }
        public IEnumerable<Parameter> TimeGrid { get; }

        public void Save(string path = @"./results.csv")
        {
            var append = false;
            using (var sw = new System.IO.StreamWriter(path, append))
            {
                string row = "TimeGrid," + string.Join(",", TimeGrid.Select(p => p.Time));
                sw.WriteLine(row);

                int numPath = Results.Count;
                Console.WriteLine(numPath);
                for (int pathIndex = 1; pathIndex <= numPath; ++pathIndex)
                {
                    var result = Results[pathIndex - 1].Select(r => r[0].Real);
                    row = "Path" + pathIndex.ToString() + ",";
                    row += string.Join(",", result);
                    sw.WriteLine(row);
                }
            }
        }
    }

    internal class Program
    {
        private static Complex ExpBS(Parameter start, Parameter end, Complex s_0, double r)
        {
            return s_0 * Math.Exp(r * (end - start).Time);
        }

        private static Complex VarBS(Parameter start, Parameter end, Complex s_0, double r, double sigma)
        {
            var av = ExpBS(start, end, s_0, r);
            return s_0 * s_0 * Math.Exp((2 * r + sigma * sigma) * (end - start).Time) - av * av;
        }

        private static Complex ExpVasicek(Parameter start, Parameter end, Complex s_0, double a, double b)
        {
            var dt = (end - start).Time;
            return s_0 * Math.Exp(-a * dt) + b * (1 - Math.Exp(-a * dt));
        }

        private static Complex VarVasicek(Parameter start, Parameter end, Complex s_0, double a, double b, double sigma)
        {
            var dt = (end - start).Time;
            return sigma * sigma * (1 - Math.Exp(-2 * a * dt)) / (2 * a);
        }

        private static void Main()
        {
            //Matrix<Complex> matrix1 = Matrix<Complex>.Build.DenseOfColumnArrays(new Complex[1][]
            //{
            //    new Complex[2]{1.0, 2.0}
            //});
            //Matrix<Complex> matrix2 = Matrix<Complex>.Build.DenseOfColumnArrays(new Complex[2][]
            //{
            //    new Complex[1]{1},
            //    new Complex[1]{2}
            //});
            //Console.WriteLine(matrix1);
            //Console.WriteLine(matrix2);
            //Console.WriteLine(matrix1 * matrix2);
            //Console.WriteLine(matrix2 * matrix1);
            // BS
            double r_BS = 0.01;
            double sigma_BS = 0.1;

            //
            double a_vasicek = 0.1;
            double b_vasicek = 0.1;
            double sigma_vasicek = 0.01;

            var F = new Func<Input, Complex>[]
            {
                delegate (Input input){return r_BS * input.Xt[0]; },
                // delegate (Input input){return r_BS * input.Xt[1]; }
                delegate (Input input){return a_vasicek * (b_vasicek - input.Xt[1]); }
            };
            var L = new Func<Input, Complex>[2][]
            {
                new Func<Input, Complex>[]
                {
                    delegate (Input input){return sigma_BS * input.Xt[0]; }, delegate(Input input){return (Complex)0.0; }
                },
                new Func<Input, Complex>[]
                {
                    // delegate(Input input){return (Complex)0.0; }, delegate (Input input){return sigma_BS * input.Xt[1]; }
                    delegate (Input input){return 5 * sigma_vasicek; }, delegate (Input input){return sigma_vasicek; }
                }
            };

            var fV = new FunctionVector<Input>(F);
            var fM = new FunctionMatrix<Input>(L);

            var start = new Parameter(0);
            var end = new Parameter(2);
            var initial = new Complex[] { (Complex)1.0, (Complex)0.0 };

            var sde = new SDE(start, initial, fV, fM);
            var cdn = new MilsteinSolverCondition(ESDESolver.Milstein, end, Enumerable.Range(0, initial.Count()).Select(t => (Complex)1.0e-10), 10);

            int steps = 100;
            Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker = delegate (Parameter st, Parameter en)
            {
                var dt = (en - st) / steps;
                return Enumerable.Range(0, steps + 1).Select(i => st + (dt * i));
            };

            var solver = SDESolverFactory.GetSDESolver(sde, cdn, timeGridMaker);

            int samples = 100;
            var results = Enumerable.Range(0, samples).Select(i =>
            {
                return solver.Solve();
            });

            var lasts1 = results.Select(result => result.Last()[0].Real);
            Console.WriteLine($"Average\t{lasts1.Average()}\nStdDev\t{lasts1.Variance()}");
            var lasts2 = results.Select(result => result.Last()[1].Real);
            Console.WriteLine($"Average\t{lasts2.Average()}\nStdDev\t{lasts2.Variance()}");

            var expectedAverageBS = ExpBS(start, end, initial[0], r_BS);
            var expectedVarianceBS = VarBS(start, end, initial[0], r_BS, sigma_BS);
            Console.WriteLine(expectedAverageBS);
            Console.WriteLine(expectedVarianceBS);
            Console.WriteLine(ExpVasicek(start, end, initial[1], a_vasicek, b_vasicek));
            Console.WriteLine(VarVasicek(start, end, initial[1], a_vasicek, b_vasicek, sigma_vasicek));

            var saver = new ResultSaver(results, timeGridMaker(start, end));
            saver.Save();
        }
    }
}
