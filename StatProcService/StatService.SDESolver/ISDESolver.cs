﻿using StatService.StochasticProcess;
using System;
using System.Numerics;
using System.Collections.Generic;
using StatService.StochasticDifferentialEquation;

namespace StatService.SDESolver
{
    public interface ISDESolver
    {
        Func<Parameter, Parameter, IEnumerable<Parameter>> TimeGridMaker { get; }

        SDE SDE { get; }

        SDESolverCondition Condition { get; }

        IEnumerable<MathNet.Numerics.LinearAlgebra.Vector<Complex>> Solve();
    }
}
