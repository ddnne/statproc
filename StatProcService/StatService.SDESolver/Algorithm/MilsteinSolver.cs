﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Accord.Math;
using MathNet.Numerics.LinearAlgebra;
using StatService.StochasticDifferentialEquation;
using StatService.StochasticProcess;
using StatService.Utils;

namespace StatService.SDESolver.Algorithm
{
    public class MilsteinSolver : EularMaruyamaSolver
    {
        public MilsteinSolver(Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker, SDE sde, SDESolverCondition condition) : base(timeGridMaker, sde, condition)
        {
        }

        /// <param name="gridStart">t_i</param>
        /// <param name="gridEnd">t_{i+1}</param>
        /// <param name="steps">to divide [t_i, t_{i+1}] by this size (decide dt as: dt *steps = t_{i+1} - t_i)</param>
        /// <returns>matrix[i, j]
        /// i: index of IEnumerable : time index i [t_0 + dt * i, t_0 + dt * (i+1)]
        /// j: index of Vector      : sample from j'th wiener at the i'th time grid
        /// </returns>
        private MathNet.Numerics.LinearAlgebra.Matrix<Complex> ComputeWiernerSamplingMatrix(Parameter gridStart, Parameter gridEnd, int steps)
        {
            int dim = SDE.L.Functions[0].Count();

            var tmp = Enumerable.Range(0, steps + 1).Select(n => gridStart + n * (gridEnd - gridStart) / steps);
            var gridEnds = tmp.Skip(1);
            var gridStarts = tmp.SkipLast(1);
            var grids = gridStarts.Zip(gridEnds, (start, end) => new { start, end });

            var normalByTimeGrid = grids.Select(grid => (new WienerProcess(grid.end, dim)) - new WienerProcess(grid.start, dim));
            var randomByTimeGrid = normalByTimeGrid.Select(normal => normal.Generate().Select(r => (Complex)r).ToList()).ToList();

            var wienerSamplingMatrix = MathNet.Numerics.LinearAlgebra.Matrix<Complex>.Build.DenseOfRows(randomByTimeGrid);

            return wienerSamplingMatrix;
        }

        /// <summary>
        /// Compute the \int^t_{i+1}_t_i (W_\tau - W_t_i) dW_\tau
        /// </summary>
        /// <param name="intStart">start point of the integral</param>
        /// <param name="intEnd"> end point of the integral</param>
        /// <param name="wienerSamplingMatrix">computed by ComputeWiernerRandomValueMatrix</param>
        /// <param name="integrationSteps">how many steps to integrate the time grid (intEnd - intStart)</param>
        /// wiener sampling matrix computed by ComputeWiernerRandomValueMatrix
        /// <returns>
        /// results matrix[i, j] :   \int (W_\tau^i - W_t_0^i) dW_\tau^j</returns>
        protected Matrix<Complex> ComputeIntegrateWtdWtTerm(Parameter intStart, Parameter intEnd, Matrix<Complex> wienerSamplingMatrix, int integrationSteps)
        {
            int wienerDim = SDE.L.Functions[0].Count();

            // make time grids for integration
            var startToEnd = (intEnd - intStart);
            var dt = startToEnd / integrationSteps;
            var starts = Enumerable.Range(0, integrationSteps).Select(i => intStart + dt * i);
            var ends = starts.Select(s => s + dt);
            var grids = starts.Zip(ends, (start, end) => (start, end));

            var wtMinusW0 = wienerSamplingMatrix.ColumnSums();
            var wienerIntegralMatrix = Matrix<Complex>.Build.DenseDiagonal(wienerDim, wienerDim, delegate (int ii) { return (wtMinusW0[ii] * wtMinusW0[ii] - startToEnd.Time) / 2; });

            for (int i = 0; i < wienerDim; ++i)
            {
                for (int j = 0; j < wienerDim; ++j)
                {
                    if (i == j)
                        continue;
                    Complex tmp = 0.0;
                    for (int k = 0; k < integrationSteps - 1; ++k)
                    {
                        tmp += Enumerable.Range(0, k + 1).Select(l => wienerSamplingMatrix[l, i]).Sum() * (wienerSamplingMatrix[k + 1, j] - wienerSamplingMatrix[k, j]);
                    }
                    wienerIntegralMatrix[i, j] = tmp;
                }
            }
            return wienerIntegralMatrix;
        }

        /// <summary>
        /// Compute \mathcal{L}_j L
        /// </summary>
        /// <param name="input">input</param>
        /// <returns>
        /// return[i][j, k] = \mathcal{L}_i L_jk
        /// </returns>
        private IEnumerable<Matrix<Complex>> ComputeMilsteinLjLTerm(Input input)
        {
            var dv = MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfEnumerable(((MilsteinSolverCondition)Condition).Infinitesimal);
            int dim = dv.Count;
            int wienerDim = SDE.L.Functions[0].Count();

            var mathcalLjL = Enumerable.Range(0, dim).Select(i =>
            {
                var dx = MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfArray(new Complex[dim]);
                var delta = dv[i];
                dx[i] = delta;

                return ((SDE.L.GetValues(new Input(input.Xt + dx, input.Time)) - SDE.L.GetValues(new Input(input.Xt - dx, input.Time))) / (2 * delta));
            });

            return mathcalLjL;
        }

        private MathNet.Numerics.LinearAlgebra.Vector<Complex> ComputeMilsteinTerm(Input input, Parameter intStart, Parameter intEnd, Matrix<Complex> wienerSamplingMatrix)
        {
            int dim = input.Xt.Count;
            int wienerDim = SDE.L.Functions[0].Count();

            var differentiateMatrice = ComputeMilsteinLjLTerm(input).ToList();
            var lMatrix = SDE.L.GetValues(input);
            var wIntegral = ComputeIntegrateWtdWtTerm(intStart, intEnd, wienerSamplingMatrix, ((MilsteinSolverCondition)Condition).WienerIntegrationSteps);

            return MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfEnumerable(Enumerable.Range(0, dim).Select(i =>
        {
            var matrix = Matrix<Complex>.Build.Dense(dim, wienerDim);
            // to compute with Trace, arrange the index
            for (int j = 0; j < dim; ++j)
            {
                for (int k = 0; k < wienerDim; ++k)
                {
                    matrix[j, k] = differentiateMatrice[j][i, k];
                }
            }

            return (matrix * wIntegral * lMatrix.Transpose()).Trace();
        }));
        }

        protected override MathNet.Numerics.LinearAlgebra.Vector<Complex> ComputeRandomTerm(Input input, Parameter start, Parameter end)
        {
            var randomMatrix = ComputeWiernerSamplingMatrix(start, end, ((MilsteinSolverCondition)Condition).WienerIntegrationSteps);

            var lMatrix = SDE.L.GetValues(input);
            var randomVector = randomMatrix.ColumnSums();
            var eularMaruyamaTerm = lMatrix * randomVector;

            var milsteinTerm = ComputeMilsteinTerm(input, start, end, randomMatrix);

            var randomTerm = eularMaruyamaTerm + milsteinTerm;
            return randomTerm;
        }
    }
}
