﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using StatService.Utils;
using StatService.StochasticDifferentialEquation;
using StatService.StochasticProcess;
using MathNet.Numerics.LinearAlgebra;

namespace StatService.SDESolver.Algorithm
{
    /// <summary>
    /// Strong Order = 1/2
    /// Weak Order = 1
    /// </summary>
    public class EularMaruyamaSolver : ISDESolver
    {
        public EularMaruyamaSolver(Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker, SDE sde, SDESolverCondition condition)
        {
            TimeGridMaker = timeGridMaker;
            SDE = sde;
            Condition = condition;
        }


        public SDE SDE { get; }

        public SDESolverCondition Condition { get; }

        public Func<Parameter, Parameter, IEnumerable<Parameter>> TimeGridMaker { get; }

        protected virtual MathNet.Numerics.LinearAlgebra.Vector<Complex> ComputeDeterministicTerm(Input input, Parameter start, Parameter end)
        {
            var deterministicTerm = SDE.F.GetValues(input) * (end - start).Time;
            return deterministicTerm;
        }

        protected virtual MathNet.Numerics.LinearAlgebra.Vector<Complex> ComputeRandomTerm(Input input, Parameter start, Parameter end)
        {
            int dim = SDE.L.Functions[0].Count();
            
            // Generate Wiener Process on the Time Grid
            var wienerAtGridStart = new WienerProcess(start, dim);
            var wienerAtGridEnd = new WienerProcess(end, dim);

            // Get Random Values during the Time Evolution by Wiener Process
            var mltivariateNormal = (wienerAtGridEnd - wienerAtGridStart);
            var randomIEnumerable = mltivariateNormal.Generate().Select(r => (Complex)r);
            var randomVector = MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfEnumerable(randomIEnumerable);
            
            var lMatrix = SDE.L.GetValues(input);
            var randomTerm = lMatrix * randomVector;

            return randomTerm;
        }

        protected IEnumerable<(Parameter start, Parameter end)> MakeGridStartEnd(Parameter startGrid, Parameter endGrid)
        {
            var timeGrid = TimeGridMaker(startGrid, endGrid);
            var gridEnd = timeGrid.Skip(1);
            var gridStart = timeGrid.SkipLast(1);
            var grids = gridStart.Zip(gridEnd, (start, end) => (start, end ));
            return grids;
        }

        public IEnumerable<MathNet.Numerics.LinearAlgebra.Vector<Complex>> Solve()
        {
            var x0 = SDE.InitialValues;
            var grids = MakeGridStartEnd(SDE.Start, Condition.End);

            var x = x0;
            yield return x;
            foreach(var grid in grids)
            {
                var start = grid.start;
                var end = grid.end;
                var input = new Input(x, start.Time);

                var dtTerm = ComputeDeterministicTerm(input, start, end);
                var randomTerm = ComputeRandomTerm(input, start, end);
                x = x + dtTerm + randomTerm;
                yield return x;
            }
            yield break;
        }
    }
}
