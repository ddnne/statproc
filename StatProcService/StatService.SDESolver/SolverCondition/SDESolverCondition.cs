﻿using StatService.StochasticProcess;

namespace StatService.SDESolver
{
    public class SDESolverCondition
    {
        public SDESolverCondition(ESDESolver eSolver, Parameter end)
        {
            ESolver = eSolver;
            End = end;
        }

        public ESDESolver ESolver { get; }
        public Parameter End { get; }
    }
}
