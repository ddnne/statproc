﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Text;
using StatService.StochasticProcess;

namespace StatService.SDESolver
{
    public class MilsteinSolverCondition : SDESolverCondition
    {
        public MilsteinSolverCondition(ESDESolver eSolver, Parameter end, IEnumerable<Complex> infinitesimal, int wienerIntegrationSteps) : base(eSolver, end)
        {
            Infinitesimal = infinitesimal;
            WienerIntegrationSteps = wienerIntegrationSteps;
        }

        public int WienerIntegrationSteps { get; }

        /// <summary>
        /// to differentiate L matrix
        /// </summary>
        public IEnumerable<Complex> Infinitesimal { get; }
    }
}
