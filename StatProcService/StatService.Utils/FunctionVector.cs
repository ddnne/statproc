﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace StatService.Utils
{
    public class FunctionVector<Input>
    {
        public FunctionVector(IEnumerable<Func<Input, Complex>> functions)
        {
            Functions = functions.ToArray();
        }
        public FunctionVector(IEnumerable<Complex> vector)
        {
            Functions = vector.Select<Complex, Func<Input, Complex>>(v => delegate (Input input) { return v; }).ToArray();
        }

        internal Func<Input, Complex>[] Functions { get; }

        public MathNet.Numerics.LinearAlgebra.Vector<Complex> GetValues(Input input)
        {
            return MathNet.Numerics.LinearAlgebra.Vector<Complex>.Build.DenseOfEnumerable(Functions.Select(func => func(input))); ;
        }

        public static FunctionVector<Input> operator +(FunctionVector<Input> func1, FunctionVector<Input> func2)
        {
            var func = func1.Functions.Zip<Func<Input, Complex>, Func<Input, Complex>, Func<Input, Complex>>(func2.Functions,
                (f1, f2) => delegate (Input input)
                {
                    return (f1(input) + f2(input));
                });
            return new FunctionVector<Input>(func.ToArray());
        }

        public static FunctionVector<Input> operator -(FunctionVector<Input> func1, FunctionVector<Input> func2)
        {
            var func = func1.Functions.Zip<Func<Input, Complex>, Func<Input, Complex>, Func<Input, Complex>>(func2.Functions,
                (f1, f2) => delegate (Input input)
                {
                    return (f1(input) - f2(input));
                });
            return new FunctionVector<Input>(func.ToArray());
        }

        public static Func<Input, Complex> operator *(FunctionVector<Input> func1, FunctionVector<Input> func2)
        {
            var func = func1.Functions.Zip<Func<Input, Complex>, Func<Input, Complex>, Func<Input, Complex>>(func2.Functions,
                (f1, f2) => delegate (Input input)
                {
                    return (f1(input) * f2(input));
                });
            return delegate (Input input)
            {
                var res = func.Select(f => f(input));
                var re = res.Sum(r => r.Real);
                var im = res.Sum(r => r.Imaginary);
                return new Complex(re, im);
            };
        }
    }
}
