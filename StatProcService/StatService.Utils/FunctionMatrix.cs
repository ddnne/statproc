﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using MathNet.Numerics.LinearAlgebra;

namespace StatService.Utils
{
    public class FunctionMatrix<Input>
    {
        public FunctionMatrix(IEnumerable<IEnumerable<Func<Input, Complex>>> functions)
        {
            Functions = functions.Select(funcs => funcs.ToArray()).ToArray();
        }
        public FunctionMatrix(IEnumerable<IEnumerable<Complex>> matrix)
        {
            Functions = matrix.Select(vector => vector.Select<Complex, Func<Input, Complex>>(v => delegate (Input input) { return v; }).ToArray()).ToArray();
        }
        public FunctionMatrix(IEnumerable<Complex[]> matrix)
        {
            Functions = matrix.Select(vector => vector.Select<Complex, Func<Input, Complex>>(v => delegate (Input input) { return v; }).ToArray()).ToArray();
        }
        public FunctionMatrix(Complex[][] matrix)
        {
            Functions = matrix.Select(vector => vector.Select<Complex, Func<Input, Complex>>(v => delegate (Input input) { return v; }).ToArray()).ToArray();
        }

        public Func<Input, Complex>[][] Functions { get; }

        public MathNet.Numerics.LinearAlgebra.Matrix<Complex> GetValues(Input input)
        {
            return Matrix<Complex>.Build.DenseOfRows(Functions.Select(funcVector => funcVector.Select(func => func(input))));
        }
        public MathNet.Numerics.LinearAlgebra.Matrix<double> GetRealValues(Input input)
        {
            return Matrix<double>.Build.DenseOfRows(Functions.Select(funcVector => funcVector.Select(func => func(input).Real)));
        }
        public MathNet.Numerics.LinearAlgebra.Matrix<double> GetImaginaryValues(Input input)
        {
            return Matrix<double>.Build.DenseOfRows(Functions.Select(funcVector => funcVector.Select(func => func(input).Imaginary)));
        }

        public static FunctionMatrix<Input> operator +(FunctionMatrix<Input> func1, FunctionMatrix<Input> func2)
        {
            var func = func1.Functions.Zip(func2.Functions, (funcVector1, funcVector2) => 
            funcVector1.Zip<Func<Input, Complex>, Func<Input, Complex>, Func<Input, Complex>>(funcVector2, (f1, f2) => delegate (Input input)
               {
                   return (f1(input) + f2(input));
               }));
            return new FunctionMatrix<Input>(func);
        }

        public static FunctionMatrix<Input> operator -(FunctionMatrix<Input> func1, FunctionMatrix<Input> func2)
        {
            var func = func1.Functions.Zip(func2.Functions, (funcVector1, funcVector2) => 
            funcVector1.Zip<Func<Input, Complex>, Func<Input, Complex>, Func<Input, Complex>>(funcVector2, (f1, f2) => delegate (Input input)
                {
                    return (f1(input) - f2(input));
                }));
            return new FunctionMatrix<Input>(func);
        }

        public static FunctionMatrix<Input> operator *(FunctionMatrix<Input> func1, FunctionMatrix<Input> func2)
        {
            var n = func1.Functions.Count();
            var m = func2.Functions[0].Count();

            var l = func1.Functions[0].Count();

            var funcMatrix = new Func<Input, Complex>[n][];
            for(int i = 0; i < n; ++i)
            {
                funcMatrix[i] = new Func<Input, Complex>[m];
                var func1Vector = new FunctionVector<Input>(Enumerable.Range(0, l).Select(k => func1.Functions[i][k]).ToArray());

                for (int j = 0; j < m; ++j)
                {
                    var func2Vector = new FunctionVector<Input>(Enumerable.Range(0, l).Select(k => func2.Functions[k][j]).ToArray());
                    funcMatrix[i][j] = func1Vector * func2Vector;
                }
            }

            return new FunctionMatrix<Input>(funcMatrix);
        }
    }
}
