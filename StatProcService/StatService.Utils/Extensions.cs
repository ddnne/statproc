﻿using System;
using System.Numerics;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace StatService.Utils
{
    public static class Extensions
    {
        public static double Variance(this IEnumerable<double> values)
        {
            var squares = values.Select(v => v * v);

            var average = values.Average();
            var squareAverage = squares.Average();

            return squareAverage - average * average;
        }

        public static double Variance(this IEnumerable<Complex> values)
        {
            var squares = values.Select(v => (Complex.Conjugate(v) * v).Real);
            var squareAverage = squares.Average();

            var realAverage = values.Select(v => v.Real).Average();
            var imagAverage = values.Select(v => v.Imaginary).Average();

            return squareAverage - (realAverage * realAverage + imagAverage * imagAverage);
        }

        public static double StdDev(this IEnumerable<double> values)
        {
            return Math.Sqrt(values.Variance());
        }

        public static double StdDev(this IEnumerable<Complex> values)
        {
            return Math.Sqrt(values.Variance());
        }

        public static Complex Sum(this IEnumerable<Complex> values)
        {
            var real = values.Select(v => v.Real).Sum();
            var imag = values.Select(v => v.Imaginary).Sum();

            return new Complex(real, imag);
        }
    }
}
