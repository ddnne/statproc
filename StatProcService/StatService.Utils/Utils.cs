﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;

namespace StatService.Utils
{
    public class Input
    {
        public Input(MathNet.Numerics.LinearAlgebra.Vector<Complex> xt, double time)
        {
            Xt = xt;
            Time = time;
        }

        public MathNet.Numerics.LinearAlgebra.Vector<Complex> Xt { get; }

        public double Time { get; }
    }

    public class UtilFunctions
    {
        public static Func<Input, Complex> ConvertArgToInput(Func<IEnumerable<Complex>, double, Complex> func)
        {
            return delegate (Input input) { return func(input.Xt, input.Time); };
        }

        public static IEnumerable<Func<Input, Complex>> ConvertArgToInput(IEnumerable<Func<IEnumerable<Complex>, double, Complex>> funcVector)
        {
            return funcVector.Select(func => ConvertArgToInput(func)).ToArray();
        }

        public static IEnumerable<IEnumerable<Func<Input, Complex>>> ConvertArgToInput(IEnumerable<IEnumerable<Func<IEnumerable<Complex>, double, Complex>>> funcMatrix)
        {
            return funcMatrix.Select(funcVector => ConvertArgToInput(funcVector)).ToArray();
        }
    }
}
