﻿using StatService.StochasticDifferentialEquation;
using StatService.SDESolver;
using System;
using System.Collections.Generic;
using System.Text;
using StatService.StochasticProcess;

namespace StatService.SDESolverBuilder
{
    public interface ISDESolverBuilder
    {
        void AddSDE(SDE sde);
        void AddSDSolverCondition(SDESolverCondition cdn);
        void AddTimeGridMaker(Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker);
        ISDESolver BuildSolver();
    }
}