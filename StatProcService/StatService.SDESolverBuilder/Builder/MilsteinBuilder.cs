﻿using System;
using System.Collections.Generic;
using System.Text;
using StatService.SDESolver;
using StatService.SDESolver.Algorithm;
using StatService.StochasticDifferentialEquation;
using StatService.StochasticProcess;

namespace StatService.SDESolverBuilder
{
    public class MilsteinBuilder : ISDESolverBuilder
    {
        private SDE SDE;
        private SDESolverCondition Condition;
        private Func<Parameter, Parameter, IEnumerable<Parameter>> TimeGridMaker;

        public void AddSDE(SDE sde) => this.SDE = sde;

        public void AddSDSolverCondition(SDESolverCondition cdn)
        {
            if(cdn is MilsteinSolverCondition)
            {
                this.Condition = cdn;
            }
            else
            {
                throw new Exception("Please use MilsteinSolverCondition");
            }
        }
        
        public void AddTimeGridMaker(Func<Parameter, Parameter, IEnumerable<Parameter>> timeGridMaker) => TimeGridMaker = timeGridMaker;

        public ISDESolver BuildSolver()
        {
            return new MilsteinSolver(TimeGridMaker, SDE, Condition);
        }
    }
}
